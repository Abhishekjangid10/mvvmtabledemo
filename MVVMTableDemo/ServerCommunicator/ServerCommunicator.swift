//
//  ServerCommunicator.swift
//  MoodTracking
//
//  Created by Shruti on 20/06/16.
//  Copyright © 2016 Vinod Sahu. All rights reserved.
//

import UIKit
import SystemConfiguration

//method for release mode to remove print for release version.
func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    Swift.print(items[0], separator:separator, terminator: terminator)
}

class ServerCommunicator: NSObject {
    
    
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    static func GetDataForMethod(serviceName : String, parameters : NSDictionary, methodType : String, controller : AnyObject ,callback :@escaping (_ response : AnyObject, _ error : NSError?)-> ())
    {
        if self.isConnectedToNetwork() == false {
            print("No internet")
            return
        }
//        Utility.showHUD()
        
        var baseUrl = "https://newsapi.org/v1/articles?source=the-next-web&sortBy=latest&apiKey=0cf790498275413a9247f8b94b3843fd"
        
        
        let request : NSMutableURLRequest!
        request = NSMutableURLRequest(url: NSURL(string: baseUrl)! as URL)
        
        let session = URLSession.shared
        request.httpMethod = methodType
        
        
        let _: NSError?
        if methodType == "POST" || methodType == "PUT" {
            let JSONData: NSData?
            do {
                
                JSONData = try! JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions(rawValue:0)) as NSData?
                let strRequest = NSString(data: JSONData! as Data, encoding: String.Encoding.utf8.rawValue)
                print("Request : \(String(describing: strRequest))")
                
            }
            request.httpBody = JSONData as Data?
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            
            print("Response: \(String(describing: response))")
            
            if data == nil{
                print("Nil response return")
                DispatchQueue.main.async {
                    //Utility.dissmissHUD()
                    //appDelegate.window?.rootViewController?.presentAlertWith(message: Constants.Error_Unexpected)
                    
                }
                //Utility.dissmissHUD()
            }
            else{
                
                let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Body: \(String(describing: strData))")
                let json : NSDictionary?
                do{
                    json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? NSDictionary
                    
                }catch _ {
                    json = nil
                }
                
                
                //callback
                if json != nil{
                    
                    let dicResponse: NSDictionary = json! as NSDictionary
                    
                    print("dicResponse \(dicResponse)")
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        print("Status code: (\(httpResponse.statusCode))")
                        let responseCode: Int = httpResponse.statusCode
                        
                        print("Succes: \(responseCode)")
                        if responseCode == 200
                        {
                            print("success get data");
                            //Utility.dissmissHUD()
                            callback(json!,error as NSError?)
                        }
                        else if responseCode == 100 || responseCode == 401 || responseCode == 404 || responseCode == 300 || responseCode == 201 || responseCode == 101 { // true
                            DispatchQueue.main.async {
                                //Utility.dissmissHUD()
                                
                            }
                            print("validation error");
                            callback(responseCode as AnyObject,error as NSError?)

                        }
                        else if responseCode == 500{
                            //Utility.dissmissHUD()
                            callback(json!,error as NSError?)
                        }

                        else{
                          // Utility.dissmissHUD()
                            callback(json!,error as NSError?)
                        }
                    }
                }
                else{
                    
                    if let httpResponse = response as? HTTPURLResponse {
                        print("Status code: (\(httpResponse.statusCode))")
                        let responseCode: Int = httpResponse.statusCode
                        if responseCode == 404
                        {
                            print("validation error");
                            callback(responseCode as AnyObject,error as NSError?)
                        }
                        else
                        {
                               // print(Constants.Error_Unexpected)
                                DispatchQueue.main.async {
                                    //Utility.dissmissHUD()
                                    //appDelegate.window?.rootViewController?.presentAlertWith(message: Constants.Error_Unexpected)
                                }

                        }
                        
                    }

                    //Utility.dissmissHUD()
                 
                }
            }
        }
        task.resume()
    }
    
    
    
    
    //Method To set Parameters as Get
    static func webServiceCallGet(methodname : String, parameter : NSDictionary, controller : AnyObject , httpType: String, completeBlock:@escaping ( _ status : Bool,  _ data : AnyObject, _ httpCode : AnyObject, _ error : NSError?)->()){
        
        if self.isConnectedToNetwork() == true
        {
            
            var baseUrl = "https://newsapi.org/v1/articles?source=the-next-web&sortBy=latest&apiKey=0cf790498275413a9247f8b94b3843fd"
            
            
//            baseUrl=baseUrl.substring(to: baseUrl.index(before: baseUrl.endIndex))
            baseUrl=baseUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print(baseUrl)
            
            let url = NSURL(string: baseUrl)
            
            let request = NSMutableURLRequest(url: url! as URL)
            request.httpMethod = httpType
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            
            request.timeoutInterval = 120;
            
            let err : NSError?
            err = nil
            if httpType == "POST" || httpType == "PUT"
            {
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parameter, options: []);
                } catch _ {
                }
            }
            do {
                if let postData : NSData = try JSONSerialization.data(withJSONObject: parameter, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData?{
                    
                    let json = NSString(data: postData as Data, encoding: String.Encoding.utf8.rawValue)! as String
                    print(json)
                    
                }
                
            }
            catch {
                print(error)
            }
            
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                let _: AutoreleasingUnsafeMutablePointer<NSError?>? = nil
                if data != nil{
                    var jsonResult: NSDictionary!
                    jsonResult = nil
                    do {
                        NSLog(String(data: data!, encoding: String.Encoding.utf8)!)
                        jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                        print(jsonResult)
                    } catch _ {
                    }
                    
                    DispatchQueue.main.async{
                        if let httpResponse = response as? HTTPURLResponse {
                            print("Status code: (\(httpResponse.statusCode))")
                            let responseCode: Int = httpResponse.statusCode
                            completeBlock(false, jsonResult, responseCode as AnyObject, err  as NSError?)
                            
                        }
                    }
                    
                }
                else
                {
                    if let httpResponse = response as? HTTPURLResponse {
                        print("Status code: (\(httpResponse.statusCode))")
                        let responseCode: Int = httpResponse.statusCode
                        
                        //Utility.dissmissHUD()
                        completeBlock(false, data as AnyObject, responseCode as AnyObject, err  as NSError?)
                    }
                }
                
                
            }
            task.resume()
        }
            
        else
        {
            //Utility.dissmissHUD()
            //UIApplication.shared.windows[1].rootViewController!.presentAlertWith(message: //Constants.NoInternet)
            return
        }
    }
    
    
    
    
}
