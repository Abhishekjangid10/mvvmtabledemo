//
//  ViewModel.swift
//  MVVMTableDemo
//
//  Created by Abhishek Jangid on 17/03/19.
//  Copyright © 2019 Abhishek Jangid. All rights reserved.
//

import UIKit

class ViewModel: NSObject {

//    var objArticle: [ModelArticle] = [ModelArticle]()
    
    var objArticle: ModelRootClass!
    
    
    func fetch(completion: @escaping () -> Void) {
        
        ServerCommunicator.webServiceCallGet(methodname: "", parameter: ["":""], controller: self, httpType: "GET") { (status, response, httpResponse, error) in
            //write  here business logic
            
            self.objArticle = ModelRootClass(fromDictionary: response as! [String : Any])
            completion()
            
        }
    }
    
    
}
