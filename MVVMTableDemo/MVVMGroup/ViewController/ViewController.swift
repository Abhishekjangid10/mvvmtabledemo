//
//  ViewController.swift
//  MVVMTableDemo
//
//  Created by Abhishek Jangid on 17/03/19.
//  Copyright © 2019 Abhishek Jangid. All rights reserved.
//

import UIKit

class cellClass: UITableViewCell {
    @IBOutlet var lbltitle: UILabel?
    
    
}

class ViewController: UIViewController {

    @IBOutlet var tbl: UITableView!
    @IBOutlet var viewModel: ViewModel!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        viewModel.fetch { [weak self] in
            self?.tbl.delegate = self;
            self?.tbl.dataSource = self;
            self?.tbl.reloadData()
        }
    }


}

