//
//  ViewControllerExtension+tableviewDataSource.swift
//  MVVMTableDemo
//
//  Created by Abhishek Jangid on 17/03/19.
//  Copyright © 2019 Abhishek Jangid. All rights reserved.
//

import UIKit

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.objArticle.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellClass", for: indexPath) as! cellClass
        cell.lbltitle?.text = viewModel.objArticle.articles[indexPath.row].title
        return cell;
    }
    
    
}
