//
//	ModelRootClass.swift
//
//	Create by Abhishek Jangid on 17/3/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelRootClass : NSObject, NSCoding{

	var articles : [ModelArticle]!
	var sortBy : String!
	var source : String!
	var status : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		articles = [ModelArticle]()
		if let articlesArray = dictionary["articles"] as? [[String:Any]]{
			for dic in articlesArray{
				let value = ModelArticle(fromDictionary: dic)
				articles.append(value)
			}
		}
		sortBy = dictionary["sortBy"] as? String
		source = dictionary["source"] as? String
		status = dictionary["status"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if articles != nil{
			var dictionaryElements = [[String:Any]]()
			for articlesElement in articles {
				dictionaryElements.append(articlesElement.toDictionary())
			}
			dictionary["articles"] = dictionaryElements
		}
		if sortBy != nil{
			dictionary["sortBy"] = sortBy
		}
		if source != nil{
			dictionary["source"] = source
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         articles = aDecoder.decodeObject(forKey :"articles") as? [ModelArticle]
         sortBy = aDecoder.decodeObject(forKey: "sortBy") as? String
         source = aDecoder.decodeObject(forKey: "source") as? String
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if articles != nil{
			aCoder.encode(articles, forKey: "articles")
		}
		if sortBy != nil{
			aCoder.encode(sortBy, forKey: "sortBy")
		}
		if source != nil{
			aCoder.encode(source, forKey: "source")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}