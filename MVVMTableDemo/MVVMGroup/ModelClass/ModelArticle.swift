//
//	ModelArticle.swift
//
//	Create by Abhishek Jangid on 17/3/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelArticle : NSObject, NSCoding{

	var author : String!
	var descriptionField : String!
	var publishedAt : String!
	var title : String!
	var url : String!
	var urlToImage : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		author = dictionary["author"] as? String
		descriptionField = dictionary["description"] as? String
		publishedAt = dictionary["publishedAt"] as? String
		title = dictionary["title"] as? String
		url = dictionary["url"] as? String
		urlToImage = dictionary["urlToImage"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if author != nil{
			dictionary["author"] = author
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if publishedAt != nil{
			dictionary["publishedAt"] = publishedAt
		}
		if title != nil{
			dictionary["title"] = title
		}
		if url != nil{
			dictionary["url"] = url
		}
		if urlToImage != nil{
			dictionary["urlToImage"] = urlToImage
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         author = aDecoder.decodeObject(forKey: "author") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         publishedAt = aDecoder.decodeObject(forKey: "publishedAt") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String
         urlToImage = aDecoder.decodeObject(forKey: "urlToImage") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if author != nil{
			aCoder.encode(author, forKey: "author")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if publishedAt != nil{
			aCoder.encode(publishedAt, forKey: "publishedAt")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}
		if urlToImage != nil{
			aCoder.encode(urlToImage, forKey: "urlToImage")
		}

	}

}